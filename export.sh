#!/bin/bash/
# Export script of the current zsh/oh-my-zsh/power10k/fzf
GIT_REPO=$(pwd)

printf "\n########## Remove old config files from repo folder ########\n"
rm $GIT_REPO/.zshrc

printf "\n########## Copy new config files ##########\n"
cp ~/.zshrc $GIT_REPO/.zshrc

