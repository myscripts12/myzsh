# [DEPRECATED] see https://gitlab.com/cybernemo/dotfiles

# myZSH

This repository contains my ZSH configuration. There is an import script and an export script. The import script will import the current configuration while the export script will export the configuration to this repo.

## Supported OS

Currently the following OSes are supported:

- [x] Centos 8
- [x] Manjaro
- [x] Ubuntu 20.04
- [x] Debian
- [x] ArchLinux

## Export

The export will export .zshrc and .p10k.zsh configuration files directly to this repo. Making it ready to be imported to another computer.

First clone this repository and within it, run:

```zsh
bash export.sh
```

## Import

The import script will install and import the ZSH configuration.

To run it:

```zsh
curl https://gitlab.com/myscripts12/myzsh/-/raw/master/import.sh | bash
```

After the import, an reboot is necessary.

## Update

The update will update the .zshrc and .p10k.zsh without reinstalling everything (especially useful to keep the zsh history)

```zsh
curl https://gitlab.com/myscripts12/myzsh/-/raw/master/update.sh | bash
```

## TODO

- [x] Add installation of Nerd Meslo Font
- [x] Add installation cancelling if OS is not defined
- [ ] Add support for Debian, Raspberry pi OS, Ubuntu 18.04
- [ ] Improve OS detection (with OS version)
