#!/bin/bash/
# This script must be run with bash (dash for instance will not accept "<<<")

OS=$(grep "^ID_LIKE=" /etc/os-release)
OS=$(sed -e 's#.*=\(\)#\1#' <<<$OS)
OS=$(sed 's/"//g' <<<$OS)

printf "The  current OS is $OS\n"

# Check if the OS corresponds to a known one, if not exit with error
[[ $OS != "manjaro" && $OS != "archlinux" && $OS != "ubuntu" && $OS != "debian" && $OS != "rhel fedora" ]] && ( printf "OS not supported\n" ; exit 1 )

printf "########## Install prerequisites ##########\n"
case $OS in
manjaro|arch)
    sudo pacman -S --noconfirm wget 
    ;;
ubuntu|debian)
    sudo apt -qq install -y wget
    ;;
"rhel fedora")
    sudo dnf install -y wget -q
    ;;
esac

printf "########## Remove previous installs ##########\n"
sudo chsh $USER -s $(which bash)

case $OS in
manjaro|archlinux)
    sudo pacman -R --noconfirm zsh
    ;;
ubuntu|debian)
    sudo apt -qq remove -y zsh
    ;;
"rhel fedora")
    sudo dnf remove -y zsh -q
    ;;
esac

rm -rf ~/.fzf*
rm -rf ~/.zshrc
cp ~/.zsh_history .zsh_history_bak

case $OS in
manjaro|archlinux)
    printf "\n########## Update pacman ##########\n"
    sudo pacman -Syyu --noconfirm
    printf "\n########## Install paru ##########\n"
    sudo pacman -S --noconfirm paru
    printf "\n########## Install zsh ##########\n"
    paru -S --noconfirm zsh
    printf "\n########## Install fzf ##########\n"
    paru -S --noconfirm fzf
    ;;
ubuntu|debian)
    printf "\n########## Update apt ##########\n"
    sudo apt -qq update -y
    printf "\n########## Install zsh ##########\n"
    sudo apt -qq install -y zsh
    printf "\n########## Install fzf ##########\n"
    sudo apt -qq install -y fzf
    ;;
"rhel fedora")
    printf "\n########## Update dnf ##########\n"
    sudo dnf update -y -q
    printf "\n########## Install zsh dependencies ##########\n"
    sudo dnf install -y ncurses-devel gcc -q
    printf "\n########## Install zsh ##########\n"
    cd /usr/local/src/
    sudo curl -L https://sourceforge.net/projects/zsh/files/zsh/5.8/zsh-5.8.tar.xz/download -o zsh-5.8.tar.xz
    sudo tar xf zsh-5.8.tar.xz
    cd zsh-5.8
    sudo ./configure && sudo make && sudo make install
    sudo grep -qxF '/usr/local/bin/zsh' /etc/shells || sudo sed -i '$ a /usr/local/bin/zsh' /etc/shells
    printf "\n########## Install fzf ##########\n"
    git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
    ~/.fzf/install --all
    source ~/.zshrc 
    ;;
esac

printf "\n########## Get zsh config ##########\n"
wget https://gitlab.com/homelab3/myzsh/-/raw/master/.zshrc -O ~/.zshrc

printf "\n########## Change to zsh ##########\n"
sudo chsh $USER -s $(which zsh)
