# Lines configured by zsh-newuser-install
HISTFILE=~/.zsh_history
HISTSIZE=100000
SAVEHIST=100000
setopt autocd nomatch notify
unsetopt beep extendedglob
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/aed/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# Preferred editor for local and remote sessions
export VISUAL='/usr/bin/nvim'
export EDITOR="$VISUAL"
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR="$VISUAL"
else
  export EDITOR="$VISUAL"
fi

# Aliases
alias nv="nvim"

# Custom Prompt
autoload -U colors && colors    # Load colors
autoload -Uz vcs_info # enable vcs_info

zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' unstagedstr '!'
zstyle ':vcs_info:*' stagedstr '+'
zstyle ':vcs_info:*' formats "%K{34}%F{0} %b %m %f%k%K{28} %k%K{22} %k%K{16} %k"
zstyle ':vcs_info:git*+set-message:*' hooks git-untracked

+vi-git-untracked() {
  if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == 'true' ]] 
  then
    unstaged_modified=$(git status --porcelain | grep 'M ' | wc -l)
    added=$(git status --porcelain | grep '^A' | wc -l)
    unstaged_deleted=$(git status --porcelain | grep 'D ' | wc -l)
    untracked=$(git status --porcelain | grep '^??' | wc -l)
    hook_com[misc]="?$untracked +$added -$unstaged_deleted !$unstaged_modified"
  fi
}

precmd() { 
  vcs_info # load git
} # always load before displaying the prompt

setopt prompt_subst # enable command substitution in the prompt

PROMPT='
/-%F{196}[%f%F{32}%n%f%F{7}@%f%F{208}%m%f%F{196}]%f%K{4}%F{15} %~%f %k%K{30} %k$vcs_info_msg_0_
\-%F{15}$%f '

RPROMPT='%*'

# Fuzzy Finder
source /usr/share/fzf/completion.zsh
source /usr/share/fzf/key-bindings.zsh

# Git
fpath=(~/.zsh/zsh-completions $fpath)

# Load syntax highlighting; should be last.
source //usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null

# End of custom configuration

