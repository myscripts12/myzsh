#!/bin/bash/
# This script must be run with bash (dash for instance will not accept "<<<")

printf "\n########## Update zsh config ##########\n"
wget https://gitlab.com/homelab3/myzsh/-/raw/master/.zshrc -O ~/.zshrc
wget https://gitlab.com/homelab3/myzsh/-/raw/master/.p10k.zsh -O ~/.p10k.zsh